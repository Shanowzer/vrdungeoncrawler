# README #

VRDungeonCrawler- All Rights Reserved. 
You may not use any material from this repository without permission from Admin

### What is this repository for? ###

* Testing of VRDungeonCrawler by PotatoFaceGames, LLC
* V 0.3

### How do I get set up? ###

* Download
* Extract
* Launch VRExpPluginExample.exe

### Bugs and broken things ###

* Dropping Weapons currently doesn't re-attach to the character. 
* Potions and other actors don't properly attach to character. 

### Controls ###

* When you spawn in, look at your controllers for in-game general controls. 

### Who do I talk to? ###

* James Nye - jamesnye@potatofacegames.com
* Cope Williams - copewilliams@potatofacegames.com